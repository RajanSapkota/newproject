import { useState } from "react";

export const Form = (props) => {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");

  const onNameChange = (e) => {
    setName(e.target.value);
  };
  const onAgeChange = (e) => {
    setAge(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    props.onformSubmit({ id: Math.random(), name, age });
    setName("");
    setAge("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <div className="fieldName">
        <label>Name</label>
        <input
          className="name"
          type="text"
          value={name}
          onChange={onNameChange}
        />
      </div>
      <div className="fieldName">
        <label>Age</label>
        <input
          className="age"
          type="number"
          value={age}
          min="1"
          onChange={onAgeChange}
        />
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};
