import { useState } from "react";
import { Form } from "./Form";
import { FormDisplay } from "./FormDisplay";

function App() {
  const [user, setUser] = useState([]);

  const handleFormSubmit = (value) => {
    setUser([...user, value]);
  };

  return (
    <div>
      <Form onformSubmit={handleFormSubmit} />
      <FormDisplay user={user} />
    </div>
  );
}

export default App;
