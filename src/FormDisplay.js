export const FormDisplay = (props) => {
  const display = (e) => {
    return (
      <p key={e.id}>
        name:{e.name}
        <br />
        age:{e.age}
      </p>
    );
  };
  return <div>{props.user?.map((e) => display(e))}</div>;
};
